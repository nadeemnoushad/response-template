# Response Template

```js
const app = require('express')()
const { responseTemplate } = require('response-template')

app.get("/", (req, res) => {
  /* code */
  res.json(responseTemplate.successTemplate("Success"))
})

app.listen(8000, () => console.log("Server running on PORT 8000"))
```

import { ERROR_MESSAGES, RESPONSE_CODES } from './methods'
// import {request, Request, response, Response, NextFunction} from 'express'

// type ResponseFunction = { [key in RESPONSE_CODE]: (data: any, meta?: any) => void }

// declare global {
//   namespace Express {
//     interface Response {
//       formatter: ResponseFunction
//     }
//   }
// }


// const responseEnhancer = () => (
//   req: Request,
//   res: Response,
//   next: NextFunction,
// ): void => {
//   res.formatter = _generateFormatters(res)
//   next()
// }

// const _generateFormatters = (res: Response) => {

// }

export const responseTemplate = {
  successTemplate(message: string) {
    return {
      status: RESPONSE_CODES.REQUEST_OK,
      message: message,
    };
  },
  serverErrorTemplate() {
    return this.errorTemplate(RESPONSE_CODES.SERVER_ERROR, ERROR_MESSAGES.SERVER_ERROR);
  },
  badRequestTemplate(error: string | object) {
    return this.errorTemplate(RESPONSE_CODES.BAD_REQUEST, error);
  },
  unAuthorizedRequestTemplate() {
    return this.errorTemplate(RESPONSE_CODES.UN_AUTHORIZED, ERROR_MESSAGES.AUTH_FAILURE);
  },
  dataNotFoundTemplate(what: string | object) {
    return this.errorTemplate(
      RESPONSE_CODES.NOT_FOUND,
      `${what} ${ERROR_MESSAGES.DATA_NOT_FOUND}`,
    );
  },
  errorTemplate(code: number, error: string | object) {
    return {
      status: code,
      error,
    };
  },
  dataTemplate(data: string | object, metadata: string) {
    return {
      status: RESPONSE_CODES.REQUEST_OK,
      data,
      metadata,
    };
  },
};

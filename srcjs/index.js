// import { ERROR_MESSAGES, RESPONSE_CODES } from './methods'
// import {Request, Response, NextFunction} from 'express'
import express from 'express';

// import { ERROR_MESSAGES, RESPONSE_CODES } from './methods';


const ERROR_MESSAGES = {
  AUTH_FAILURE: "Authentication Failed",
  SERVER_ERROR: "Server failed to process your request",
  DATA_NOT_FOUND: "Not Found",
}

const RESPONSE_CODES = {
  REQUEST_OK: 200,
  BAD_REQUEST: 400,
  SERVER_ERROR: 500,
  UN_AUTHORIZED: 401,
  NOT_FOUND: 404,
};

const methods = [
  {
    name: "ok",
    code: "200",
    message: "OK",
    isSuccess: true
  },
  {
    name: "created",
    code: "201",
    message: "Created",
    isSuccess: true
  },
  {
    name: 'notFound',
    code: '404',
    message: 'Not Found',
    isSuccess: false,
  },
  {
    name: 'unAuthorized',
    code: '401',
    message: 'Unauthorized',
    isSuccess: false,
  },
]




/** @type {import("express").RequestHandler} */
const responseEnhancer = (req, res, next) => {
  generateFormatters(res)
  next()
}

const generateFormatters = (res) => {
  const formatters = {}
  let responseBody = {}

  methods.map(method => {
    if(method.isSuccess){
      // Handle success codes
      formatters[method.name] = (data, meta) => {
        responseBody = responseTemplate.successTemplate(method.message)
        res.status(parseInt(method.code)).json(responseBody)
      }
    }else{
      // Handle error codes
      formatters[method.name] = (data, meta) => {
        responseBody = responseTemplate.errorTemplate(method.code)
        res.status(parseInt(method.code)).json(responseBody)
      }
    }
  })
  

  return formatters
}

const responseTemplate = {
  successTemplate(message) {
    return {
      status: RESPONSE_CODES.REQUEST_OK,
      message: message,
    };
  },
  serverErrorTemplate() {
    return this.errorTemplate(RESPONSE_CODES.SERVER_ERROR, ERROR_MESSAGES.SERVER_ERROR);
  },
  badRequestTemplate(error) {
    return this.errorTemplate(RESPONSE_CODES.BAD_REQUEST, error);
  },
  unAuthorizedRequestTemplate() {
    return this.errorTemplate(RESPONSE_CODES.UN_AUTHORIZED, ERROR_MESSAGES.AUTH_FAILURE);
  },
  dataNotFoundTemplate(what) {
    return this.errorTemplate(
      RESPONSE_CODES.NOT_FOUND,
      `${what} ${ERROR_MESSAGES.DATA_NOT_FOUND}`,
    );
  },
  errorTemplate(code) {
    return {
      status: code,
      error,
    };
  },
  dataTemplate(data, metadata) {
    return {
      status: RESPONSE_CODES.REQUEST_OK,
      data,
      metadata,
    };
  },
};

export default { responseEnhancer }
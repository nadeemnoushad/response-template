const RESPONSE_CODES = {
  REQUEST_OK: 200,
  BAD_REQUEST: 400,
  SERVER_ERROR: 500,
  UN_AUTHORIZED: 401,
  NOT_FOUND: 404,
};

const ERROR_MESSAGES = {
  AUTH_FAILURE: "Authentication Failed",
  SERVER_ERROR: "Server failed to process your request",
  DATA_NOT_FOUND: "Not Found",
};

module.export = {ERROR_MESSAGES, RESPONSE_CODES}